package br.com.devmil.authservice.domain.user;

import java.util.Collection;
import java.util.HashSet;

import org.hibernate.annotations.ManyToAny;

import br.com.devmil.authservice.domain.app.App;
import br.com.devmil.authservice.domain.app.Role;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(unique = true, length = 20, nullable = false)
    private String username;

    @Column(length = 40, nullable = false)
    private String password;

    @Column(unique = true, nullable = false, length = 120)
    private String email;

    private String phone;

    private String address;

    private String city;

    private String state;

    @OneToMany(mappedBy = "user")
    private Collection<App> apps = new HashSet<>();

}
