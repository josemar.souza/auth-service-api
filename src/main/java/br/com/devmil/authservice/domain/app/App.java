package br.com.devmil.authservice.domain.app;

import java.util.Collection;
import java.util.HashSet;

import br.com.devmil.authservice.domain.user.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "app", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id", "name" }) })
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class App {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(name = "cliente_id", unique = true)
    private String clientId;

    @Column(name = "url_redirect", nullable = false)
    private String urlRedirect; // Default redirect url

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "app", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Collection<Role> roles = new HashSet<>();
}
