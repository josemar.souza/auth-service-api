package br.com.devmil.authservice.domain.app;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "role")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    private String authority; // ROLE_ADMIN, ROLE_USER, ROLE_CLIENT

    private String scope; //

    private String authorizedResources; //

    private String redirectUrl; // Url redirect for especific role

    @ManyToOne
    @JoinColumn(name = "app_id", nullable = false)
    private App app;
}
