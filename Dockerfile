FROM maven AS build
LABEL MAINTAINER="josemar.souza@live.com"
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN ./mvnw clean package

FROM openjdk:18-jdk-slim
COPY --from=build /usr/src/app/target/api-0.0.1-SNAPSHOT.jar /opt/app/api-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app/
EXPOSE 8080
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=prod","-Dserver.port=8080","api-0.0.1-SNAPSHOT.jar"]