![](https://gitlab.com/uploads/-/system/project/avatar/45798479/password.png?width=64)
# Auth Service API  
RESTful API for generic user authentication service  
  
![Java](https://img.shields.io/badge/java-007396.svg?style=for-the-badge&logo=oracle&logoColor=white)
![Spring](https://img.shields.io/badge/spring-%236DB33F.svg?style=for-the-badge&logo=spring&logoColor=white)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)
![Maven](https://img.shields.io/badge/maven-862474.svg?style=for-the-badge&logo=apache&logoColor=white)
![GitLab](https://img.shields.io/badge/gitlab-%23fb8500.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
  
> _Status:_ **🚧  Auth Service API 🚀 Development  🚧**  
> _Current version API endpoints:_ **v0**
  
## Tools and technology
The following technologies and frameworks were used to develop the project:  
* [Java](https://docs.oracle.com/javase/7/docs/technotes/guides/language/)
* [Spring Boot](https://spring.io/)  
* [Spring Data JPA](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)  
* [Spring Security](https://docs.spring.io/spring-security/reference/index.html)  
* [Maven](https://maven.apache.org/index.html)
* [PostgreSQL](https://www.postgresql.org)
* [GitLab](https://about.gitlab.com/)
* [Docker](https://docs.docker.com/)
  
## Features
### Implemented features
The API provides the following functionality through its endpoints: 
* ### App  
  * Create new app  
    `POST  /app HTTP:1.1`  
  * Retrieve list of apps  
    `GET  /app HTTP:1.1`  
  * Retrieve app data   
    `GET  /app/{id} HTTP:1.1`  
  * Retrieve roles of app  
    `POST  /app/{id}/role HTTP:1.1`  
  * Remove role from app  
    `DELETE  /app/{id}/role/{role_id} HTTP:1.1`  
* ### User  
  * Create new user  
    `POST  /user HTTP:1.1`  
  * Retrieve list of users  
    `GET  user HTTP:1.1`  
  * Retrieve user data  
    `GET  /user/{id} HTTP:1.1`  
  * Add app for user  
    `POST  /user/{id}/app HTTP:1.1`
  * Retrieve apps for user  
    `GET  /user/{id}/app HTTP:1.1`
  * Add role app to user  
    `POST  /user/{id}/app/{app_id} HTTP:1.1`
  * Remove role app from user  
    `DELETE  /user/{id}/app/{app_id}/{role_id} HTTP:1.1`
  * Get activity from user  
    `GET  /user/{id}/activity HTTP:1.1`  
* ### Auth  
  * Authenticate user in app  
    `POST  /auth/app/{app_id} HTTP:1.1`  
  * Logout user  
    `PATCH  /auth/app/{app_id}/user/{user_id}/logout HTTP:1.1`  
  * Request password reset  
    `POST  /auth/reset-password HTTP:1.1`  
  * Reset password user  
    `POST  /auth/reset-password/{user_id}/{token} HTTP:1.1`  
  
### Future features
These features will be developed soon (any help is welcome ❤️):
* Send email with link when change password user
  
## Getting started
Before starting, you will need to have the following tools installed on your machine:
* [Java™ Development Kit (JDK) 17+](https://www.oracle.com/java/technologies/downloads/)
* [Git 2.25+](https://git-scm.com)
* [Apache Maven 3.8.6+](https://maven.apache.org/download.cgi)
  
Also it's nice to have an editor to work with the code like  [VSCode](https://code.visualstudio.com/).
  
## Quick start
```
# Clone this repository
$ git clone https://gitlab.com/josemar.souza/auth-service-api.git

# Access project folder in terminal/cmd
$ cd auth-service-api

# Clean and install
$ ./mvnw clean install

# Run the API 
$ ./mvnw spring-boot:run

# The server will start on port:8080 
# Access http://localhost:8080/v0/swagger-ui.html to consult the endpoints documentation.
```
## Run with docker image
You need to have Docker installed on your machine. Access this tutorial to learn how to install docker [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)
```
# Pull the image from DockerHub
$ docker pull jmsouzadev/auth-service-api

# Run the container
$ docker run -p 8080:8080 auth-service-api

# The server will start on port:8080 
# Access http://localhost:8080/v0/swagger-ui.html to consult the endpoints documentation.
```

## Endpoints documentation
The detailed documentation of each of the endpoints can be consulted in the OpenAPI standard (Swagger 2.0). To do so, after running the application, access the url [http://localhost:8080/v0/swagger-ui.html](http://localhost:8080/v0/swagger-ui.html).

## Next steps
To improve the project and complete all future features, you will need:
* Expand test coverage
* Document the code